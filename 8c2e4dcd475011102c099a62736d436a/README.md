# Welcome to TeamTwo's supporting resource documents 

This is extended content alongside our 5 July 2022 presentation to invited guests from QA and Accenture.

During the course of this sprint we created 14 files in total. They feature a variety of stages of completion but a selection are included here to help communicate our process, provide a team archive of our works and highlight to our invited guests this ServiceNow app's linked Gitlab app integration.

To download all these security cleared files, please use the following link to our [OneDrive folder](https://1drv.ms/u/s!AgXO9BjWGLBEgZtYaNNg7kg2U_BggQ?e=JKjdz0). Link expires 31/07/2022. Contact one of the team to gain later access.

From app design breadcrumbs:

- [Appraisals colour palette](https://1drv.ms/b/s!AgXO9BjWGLBEgZtmngsfpy-NOX32AA?e=jpJT2g)
- [TeamTwo logo](https://1drv.ms/u/s!AgXO9BjWGLBEgZthZ4Z0OuOWaPVuFg?e=M2CjlS)

Flow design:

- [Apprisals business logic](https://1drv.ms/u/s!AgXO9BjWGLBEgZtfMhMhw4pgamj4HA?e=dB2bbb)
- [Workflow notes](https://1drv.ms/w/s!AgXO9BjWGLBEgZtaTVAfo4SjTGjh7Q?e=5uxahB)

To our daily diary:

- [ideapad](https://1drv.ms/w/s!AgXO9BjWGLBEgZteUWiqvv0I_pNAbQ?e=bg7bxv)

Updates to our super helpful TA's:

- [Daily update](https://1drv.ms/w/s!AgXO9BjWGLBEgZtbGviNPYk--Ydbhg?e=RJFt01) just a snapshot of our comms to our trainers.

And several research resources:

- Reed employment on [appraisal basics](https://www.reed.co.uk/career-advice/appraisals-what-you-need-to-know/)
- [Strategy](https://1drv.ms/w/s!AgXO9BjWGLBEgZtgoM8hJRCrvThSSg?e=V3usEo) pulled together learning path, expected outputs and ebook MUST's.

And even ServiceNow supplied apps and templates used as inpsiration or as is:

- [Agile Dev 2.0](https://docs.servicenow.com/en-US/bundle/sandiego-it-business-management/page/product/agile-development/reference/agile-landing-page.html)
- [Performace appraisal template](https://store.servicenow.com/sn_appstore_store.do#!/store/application/91256a9899862010f8779b076766f4fb/21.1.3)

We put early thought into support documentation:

- [Draft documentation](https://1drv.ms/w/s!AgXO9BjWGLBEgZtd_9n1WQXohftTBA?e=gHUoDD)

